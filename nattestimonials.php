/*
Plugin Name: Nat testimonials
Plugin URI: none
Description: This plugin allows front-end forms to collect verified users testimonials
Version: 20200409
Author: Nathanaël VERGNE (nvergne.labo-ve.fr)
Author URI: nvergne.labo-ve.fr
License: GPLv2
*/

//Define path and URL to the current plugin.

define('NV_TEMOIGNAGES_PATH', plugin_dir_path{__FILE__});

//Define path and URL to the ACF plugin

define('NV_ACF_PATH', NV_TEMOIGNAGES_PATH . "includes/acf/");
define('NV_ACF_URL', plugin_dir_url{__FILE__} . "includes/acf/");

//Include the acf plugin
include_once(NV_ACF_PATH . 'acf.php');

//Custoomize the url settings to fix incorrect asset URLs
function nv_acf_settings_url($url) {
    return NV_ACF_URL;
}

add_filter('acf/settings/url', 'nv_acf_settings_url');

//(Optional) Hide acf admin menu item
function nv_acf_settings_show_admin($show_admin){
    return false;
}

add_filter('acf/settings/show_admin', 'nv_acf_settings_show_admin');

if ( ! function_exists('nv_temoignages_cpt') ) {

// Register Custom Post Type
function nv_temoignages_cpt() {

$labels = array(
'name' => _x( 'temoignages', 'Post Type General Name', 'nv_temoignages_text' ),
'singular_name' => _x( 'temoignage', 'Post Type Singular Name', 'nv_temoignages_text' ),
'menu_name' => __( 'temoignage', 'nv_temoignages_text' ),
'name_admin_bar' => __( 'temoignage', 'nv_temoignages_text' ),
'archives' => __( 'temoignages Archives', 'nv_temoignages_text' ),
'attributes' => __( 'temoignages Attributes', 'nv_temoignages_text' ),
'parent_item_colon' => __( 'Parent temoignage:', 'nv_temoignages_text' ),
'all_items' => __( 'All temoignage', 'nv_temoignages_text' ),
'add_new_item' => __( 'Add New temoignage', 'nv_temoignages_text' ),
'add_new' => __( 'Add New', 'nv_temoignages_text' ),
'new_item' => __( 'New temoignage', 'nv_temoignages_text' ),
'edit_item' => __( 'Edit temoignage', 'nv_temoignages_text' ),
'update_item' => __( 'Update temoignage', 'nv_temoignages_text' ),
'view_item' => __( 'View temoignage', 'nv_temoignages_text' ),
'view_items' => __( 'View temoignage', 'nv_temoignages_text' ),
'search_items' => __( 'Search temoignage', 'nv_temoignages_text' ),
'not_found' => __( 'Not found', 'nv_temoignages_text' ),
'not_found_in_trash' => __( 'Not found in Trash', 'nv_temoignages_text' ),
'featured_image' => __( 'Featured Image', 'nv_temoignages_text' ),
'set_featured_image' => __( 'Set featured image', 'nv_temoignages_text' ),
'remove_featured_image' => __( 'Remove featured image', 'nv_temoignages_text' ),
'use_featured_image' => __( 'Use as featured image', 'nv_temoignages_text' ),
'insert_into_item' => __( 'Insert into temoignage', 'nv_temoignages_text' ),
'uploaded_to_this_item' => __( 'Uploaded to this temoignage', 'nv_temoignages_text' ),
'items_list' => __( 'temoignages list', 'nv_temoignages_text' ),
'items_list_navigation' => __( 'temoignages list navigation', 'nv_temoignages_text' ),
'filter_items_list' => __( 'Filter temoignages list', 'nv_temoignages_text' ),
);
$rewrite = array(
'slug' => 'natemoignage',
'with_front' => true,
'pages' => true,
'feeds' => true,
);
$args = array(
'label' => __( 'temoignage', 'nv_temoignages_text' ),
'description' => __( 'le super type temoignage de CPT de NV', 'nv_temoignages_text' ),
'labels' => $labels,
'supports' => array( 'title', 'editor' ),
'taxonomies' => array( 'category', 'post_tag' ),
'hierarchical' => false,
'public' => true,
'show_ui' => true,
'show_in_menu' => true,
'menu_position' => 20,
'menu_icon' => 'dashicons-smiley',
'show_in_admin_bar' => false,
'show_in_nav_menus' => true,
'can_export' => true,
'has_archive' => true,
'exclude_from_search' => true,
'publicly_queryable' => true,
'rewrite' => $rewrite,
'capability_type' => 'post',
);
register_post_type( 'nv_temoignage', $args );

}
add_action( 'init', 'nv_temoignages_cpt', 0 );

}

//ACF
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
'key' => 'group_5e8f2c1b54dea',
'title' => 'Témoignages',
'fields' => array(
array(
'key' => 'field_5e8f2c2a887e6',
'label' => 'Last Name',
'name' => 'nv_temoignage_last_name',
'type' => 'text',
'instructions' => '',
'required' => 1,
'conditional_logic' => 0,
'wrapper' => array(
'width' => '',
'class' => '',
'id' => '',
),
'default_value' => '',
'placeholder' => '',
'prepend' => '',
'append' => '',
'maxlength' => 40,
),
array(
'key' => 'field_5e8f2d62887e7',
'label' => 'First Name',
'name' => 'nv_temoignage_first_name',
'type' => 'text',
'instructions' => '',
'required' => 1,
'conditional_logic' => 0,
'wrapper' => array(
'width' => '',
'class' => '',
'id' => '',
),
'default_value' => '',
'placeholder' => '',
'prepend' => '',
'append' => '',
'maxlength' => 40,
),
array(
'key' => 'field_5e8f2d81887e8',
'label' => 'Email',
'name' => 'nv_temoignage_email',
'type' => 'email',
'instructions' => '',
'required' => 1,
'conditional_logic' => 0,
'wrapper' => array(
'width' => '',
'class' => '',
'id' => '',
),
'default_value' => '',
'placeholder' => '',
'prepend' => '',
'append' => '',
),
array(
'key' => 'field_5e8f2d9e887e9',
'label' => 'invoiceref',
'name' => 'nv_temoignage_invoiceref',
'type' => 'text',
'instructions' => '',
'required' => 1,
'conditional_logic' => 0,
'wrapper' => array(
'width' => '',
'class' => '',
'id' => '',
),
'default_value' => '',
'placeholder' => 'E.g : 123ABC',
'prepend' => '',
'append' => '',
'maxlength' => '',
),
array(
'key' => 'field_5e8f2de3887ea',
'label' => 'Score',
'name' => 'nv_temoignage_score',
'type' => 'range',
'instructions' => '',
'required' => 1,
'conditional_logic' => 0,
'wrapper' => array(
'width' => '',
'class' => '',
'id' => '',
),
'default_value' => 3,
'min' => 1,
'max' => 5,
'step' => 1,
'prepend' => '',
'append' => '',
),
array(
'key' => 'field_5e8f2e08887eb',
'label' => 'Review',
'name' => 'nv_temoignage_review',
'type' => 'textarea',
'instructions' => '',
'required' => 1,
'conditional_logic' => 0,
'wrapper' => array(
'width' => '',
'class' => '',
'id' => '',
),
'default_value' => '',
'placeholder' => '',
'maxlength' => '',
'rows' => '',
'new_lines' => 'wpautop',
),
array(
'key' => 'field_5e8f2e43887ec',
'label' => 'Photo',
'name' => 'nv_temoignage_photo',
'type' => 'image',
'instructions' => 'Mini : 200*200 px
Maxi : 1000*1000 px
Only GIF, JPG and JPEG',
'required' => 0,
'conditional_logic' => 0,
'wrapper' => array(
'width' => '',
'class' => '',
'id' => '',
),
'return_format' => 'array',
'preview_size' => 'full',
'library' => 'all',
'min_width' => 200,
'min_height' => 200,
'min_size' => '',
'max_width' => 1000,
'max_height' => 1000,
'max_size' => 4,
'mime_types' => 'jpg.jpeg.gif',
),
),
'location' => array(
array(
array(
'param' => 'post_type',
'operator' => '==',
'value' => 'nv_temoignages_cpt',
),
),
),
'menu_order' => 0,
'position' => 'normal',
'style' => 'default',
'label_placement' => 'top',
'instruction_placement' => 'label',
'hide_on_screen' => '',
'active' => true,
'description' => '',
));

endif;